#ifndef CORRECTOR_H
#define CORRECTOR_H

#include <iostream>
#include <QtOpenGL/QGLWidget>
#include "uniform.h"
#include "scenewidget.h"
#include "camera.h"
#include "texturewidget.h"

using namespace std;

struct Test;

class Corrector
{
public:
    static Corrector* instance();
    void parseArgs(int argc, char* argv[]);

    string vertexShader() const;
    string geometryShader() const;
    string fragmentShader() const;
    bool amGrading() const;

    void postFrame();

    void updateUniforms(QVector<CUniform>& uniforms);

    void setScene(CSceneWidget* sceneWidget);
    void setGlWidget(QGLWidget* widget);
    void setTextureWidget(CTextureWidget* textureWidget);

    void logText(const QString& text);

    void setCamera(ICameraState* cameraState);

    void preFrame();


protected:
    Corrector();
private:
    static Corrector* minstance;

    void readTest(ifstream& f);

    string mvertexShader, mgeometryShader, mfragmentShader;

    int mcurrentTest;
    vector<Test*> mtests;

    CSceneWidget *msceneWidget;
    QGLWidget* mglWidget;
    CTextureWidget* mtextureWidget;

    bool mexitOnFinish;
    bool grading;

};

#endif
