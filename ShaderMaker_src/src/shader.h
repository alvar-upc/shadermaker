//=============================================================================
/** @file		shader.cpp
 *
 * Implements the IShader interface.
 *
	@internal
	created:	2007-10-24
	last mod:	2008-02-27

    Shader Maker - a cross-platform GLSL editor.
    Copyright (C) 2007-2008 Markus Kramer
    For details, see main.cpp or COPYING.

=============================================================================*/

#ifndef __SHADER_H_INCLUDED__
#define __SHADER_H_INCLUDED__


#include <QtCore/QTime>
#include "application.h"
#include "shader_base.h"



//=============================================================================
//	IShader implementation
//=============================================================================

/** Implementation of IShader.
 * This class doensn't add new functionality. It is only used to hide
 * the class definition from the interface definition.
 */
class CShader : public IShader
{
 Q_OBJECT
public:
	/** Constructs a shader object. */
	CShader( void );
	virtual ~CShader( void ); ///< Destructor.

	// initialization
	bool init( void );
	void shutdown( void );

	// rendering
	bool bindState( VertexAttribLocations & attribs );

	// shader state
	void setShaderSource( int shaderType, const QString & source );
	void setGeometryInputType( int type );
	void setGeometryOutputType( int type );

	// linking
	bool compileAndLink( void );
	void deactivateProgram( void );
	QString getBuildLog( void );

	// type management
	bool isShaderTypeAvailable( int type );

	// IUniformState
	int getActiveUniforms( void );
	CUniform getUniform( int index );
	void setUniform( int index, const CUniform & u );

  // positions of special uniforms in m_activeUniforms:
  int m_indx_vp;
  int m_indx_mouse;
  
 signals:
  void linkedShader();
 private:

	// Does actual compile and link work.
	// Is is encapsulated into a try/catch block for driver exceptions!
	bool compileAndLink2( void );
	bool compileAndAttachShader( int shaderType );
	bool linkAndValidateProgram( void );
	void setupProgramParameters( void );

	// logs additional linking info
	void logActiveAttributes( void );
	void logActiveUniforms( void );

	// sets initial uniform state after linking
	void setupInitialUniforms( void );
	void setupRememberedUniformState( void );

	// checks wether this is the time variable and updates it.
	void updateTimeVariable( CUniform & u );

	// shader type symbol mapping
	int toGlShaderType( int symbol );

	// state of the uniforms
	QVector< CUniform > m_activeUniforms;

	// from last state
	// -> only updated if the program was successfully linked!
	QVector< CUniform > m_oldUniforms;

	// vertex and fragment shaders are included in OpenGL 2.0
	// -> geometry shader is still an extension
	bool m_geometryShaderAvailable;

	// wether the program is ready to operate
	bool m_linked;

	// results of compile and link operation
	QString m_log;

	// timer for the "uniform float time"
	QTime m_timer;

	// program parameters
	int m_geometryInputType; // for geometry shader
	int m_geometryOutputType;

	// where vertex arrays can write to...
	VertexAttribLocations m_attribLocations;

	// source code for each shader type.
	QString m_shaderSources[ MAX_SHADER_TYPES ];

	// objects
	GLuint	m_shaders[ MAX_SHADER_TYPES ];
	GLuint	m_program;
};

#endif
