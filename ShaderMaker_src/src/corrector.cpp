#include <fstream>
#include <string>
#include <sstream>
#include "scene.h"
#include "model.h"
#include "shader.h"
#include "texture.h"
#include "corrector.h"

#if DEBUG
#   include <iostream>
#endif

enum UniformType  {INT_TYPE, FLOAT_TYPE, BOOL_TYPE}; 

struct MyUniform
{
    string name;
    UniformType type;   
    int ivalues[4];
    float fvalues[4];
    bool bvalues[4];
};

struct Test
{
  Test() : sceneIndex(3), theta(0.0f), psi(0.0), phi(0.0), tx(0.f), ty(0.f), tz(-4.f), textureFilename0(""), textureFilename1(""), textureFilename2(""), textureFilename3(""), wireframe(false), vertexDensity(5)  {}

    int sceneIndex;
    string testName;
    vector<MyUniform> uniforms; // uniforms
    float theta, psi, phi;   // rotacion camara
    float tx, ty, tz;   // translacion camara
    string textureFilename0, textureFilename1, textureFilename2, textureFilename3; 
    bool wireframe;
    float backgroundRed, backgroundGreen, backgroundBlue; // glClearColor
  string meshFilename;
  int vertexDensity;
};

Corrector* Corrector::minstance = 0;

Corrector* Corrector::instance()
{
    if (minstance == 0) minstance = new Corrector;
    return minstance;
}

Corrector::Corrector()  : mvertexShader(""), mgeometryShader(""), mfragmentShader(""), mcurrentTest(0), mexitOnFinish(true), grading(false)
{}

bool Corrector::amGrading() const
{
    return grading;
}

string Corrector::vertexShader() const
{
    return mvertexShader;
}

string Corrector::geometryShader() const
{
    return mgeometryShader;
}

string Corrector::fragmentShader() const
{
    return mfragmentShader;
}

void Corrector::readTest(ifstream& f)
{
    Test* t=new Test;
    f >> t->testName;
    string s="";
    while (s!="{") f >> s;
    while (s!="}")
    {
        f >> s;
        if (s=="int") 
        {
            MyUniform unif;
            unif.type=INT_TYPE;
            f >> unif.name;
            f >> unif.ivalues[0] >> unif.ivalues[1] >> unif.ivalues[2] >> unif.ivalues[3];
            t->uniforms.push_back(unif);
        }
        if (s=="float") 
        {
            MyUniform unif;
            unif.type=FLOAT_TYPE;
            f >> unif.name;
            f >> unif.fvalues[0] >> unif.fvalues[1] >> unif.fvalues[2] >> unif.fvalues[3];
            t->uniforms.push_back(unif);
        }
        if (s=="bool") 
        {
            MyUniform unif;
            unif.type=BOOL_TYPE;
            f >> unif.name;
            f >> unif.bvalues[0] >> unif.bvalues[1] >> unif.bvalues[2] >> unif.bvalues[3]; // comprobar!!
            t->uniforms.push_back(unif);
        }
        if (s=="theta")
        {
            f >> t->theta;
        }
        if (s=="psi")
        {
            f >> t->psi;
        }
	if (s=="phi")
	{
	  f >> t->phi;
	}
        if (s=="translate")
        {
            f >> t->tx >> t->ty >> t->tz;
        }
        if (s=="scene")
        {
            f >> t->sceneIndex;
        }
        if (s=="unit0")
        {
            f >> t->textureFilename0;
            t->textureFilename0 = t->textureFilename0.substr(1, t->textureFilename0.size()-2); // delete quotes
        }
        if (s=="unit1")
        {
            f >> t->textureFilename1;
            t->textureFilename1 = t->textureFilename1.substr(1, t->textureFilename1.size()-2); // delete quotes
        }
        if (s=="unit2")
        {
            f >> t->textureFilename2;
            t->textureFilename2 = t->textureFilename2.substr(1, t->textureFilename2.size()-2); // delete quotes
        }
        if (s=="unit3")
        {
            f >> t->textureFilename3;
            t->textureFilename3 = t->textureFilename3.substr(1, t->textureFilename3.size()-2); // delete quotes
        }
        if (s=="wire")
        {
            t->wireframe = true;
        }
        if (s=="solid")
        {
            t->wireframe = false;
        }
        if (s=="background")
        {
            f >> t->backgroundRed >> t->backgroundGreen >> t->backgroundBlue;
        }
	if (s=="mesh")
	{
	  f >> t->meshFilename;
	  t->meshFilename = t->meshFilename.substr(1, t->meshFilename.size()-2); // delete quotes
	}
	if (s=="vertexDensity")
	{
	  f >> t->vertexDensity;
	}
    }
    mtests.push_back(t);
}

void Corrector::parseArgs(int argc, char* argv[])
{
    if (argc == 1) return;
    // parse Args
    if (argc!=4)
    {
        cerr << "shaderMaker shader.test shader VGFE" << endl;
        exit(-1);
    }
    
    grading = true;
    // base name for shaders
    std::string baseName(argv[2]);

    QString shaders(argv[3]);

    if (shaders.contains("V"))
    {
        mvertexShader=baseName+".vert";
    }

    if (shaders.contains("G"))
    {
        mgeometryShader=baseName+".geom";
    }

    if (shaders.contains("F"))
    {
        mfragmentShader=baseName+".frag";
    }

    mexitOnFinish = shaders.contains("E");

    // read config file
    ifstream f(argv[1]);
    if (!f.is_open())
    {
        cerr << "Cannot find " << argv[1] << endl;
        exit(-1);
    }
    while (!f.eof())
    {
        string cmd;
        f >> cmd;
        if (cmd=="vs")
        {
            f >> mvertexShader;
        }
        else if (cmd=="gs")
        {
            f >> mgeometryShader;
        }
        else if (cmd=="fs")
        {
            f >> mfragmentShader;
        }
        else if (cmd=="test")
        {
            readTest(f);
        }
    }
}

void Corrector::postFrame()
{
    if (msceneWidget->m_models and mcurrentTest < int(mtests.size()))
    {
        stringstream s;
        s << "output" << QString::number(mcurrentTest+1).toAscii().data() << ".png";    
        QImage img=mglWidget->grabFrameBuffer();
        img.save(s.str().c_str());
#if DEBUG
	std::cerr << "Saved image " << s.str() << std::endl;
#endif
        ++mcurrentTest;
    }
    else
    {
        if (msceneWidget->m_models and mexitOnFinish) exit(0);
    }
}

void Corrector::setGlWidget(QGLWidget* widget)
{
    mglWidget = widget;
}

void Corrector::updateUniforms(QVector<CUniform>& uniforms)
{
    if (mcurrentTest < int(mtests.size()))
    {
        Test* test = mtests[min(int(mtests.size())-1, mcurrentTest)];
        for (int i=0; i<uniforms.size(); ++i)
        {
            for (unsigned int j=0; j<test->uniforms.size(); ++j)
            {
                if (uniforms[i].getName() == QString(test->uniforms[j].name.c_str())) 
                {
                    for (int c=0; c<4; ++c)
                    {
                        if (test->uniforms[j].type == FLOAT_TYPE) uniforms[i].setValueAsFloat(c, test->uniforms[j].fvalues[c]);
                        else if (test->uniforms[j].type == BOOL_TYPE) uniforms[i].setValueAsBool(c, test->uniforms[j].bvalues[c]);
                        else uniforms[i].setValueAsInt(c, test->uniforms[j].ivalues[c]);
                    }
                }
            }
        }
#if DEBUG
	std::cerr << "Updated uniforms for test " << mcurrentTest << std::endl;
#endif
    }
}

void Corrector::setScene(CSceneWidget* sceneWidget)
{
    msceneWidget = sceneWidget;
}

void Corrector::logText(const QString& text)
{
    cout << text.toAscii().data() << endl;
}

void Corrector::setCamera(ICameraState* cameraState)
{
    if (mcurrentTest < int(mtests.size()))
    {
        Test* test = mtests[min(int(mtests.size())-1, mcurrentTest)];

        mat4_t m;
	//cameraState->getCameraRotation( m );

	// use the matrix stack for this multiplication...
	glPushMatrix();
	glLoadIdentity();

	// apply new rotation after old rotation
	glRotatef( test->phi, 0.0f, 0.0f, 1.0f);
	glRotatef( test->psi, 1.0f, 0.0f, 0.0f );
	glRotatef( test->theta, 0.0f, 1.0f, 0.0f );
	//glMultMatrixf( m.toConstFloatPointer() );
	
	// state update...
	glGetFloatv( GL_MODELVIEW_MATRIX, m.toFloatPointer() );
	cameraState->setCameraRotation( m );
	glPopMatrix();

        vec3_t t(test->tx,test->ty,test->tz);
        cameraState->setCameraTranslation( t );
#if DEBUG 
	std::cerr << "Set camera for test " << mcurrentTest << std::endl;
#endif
    }
}

void Corrector::preFrame()
{
    if (msceneWidget->m_models and mcurrentTest < int(mtests.size()))
    {
        Test* test = mtests[min(int(mtests.size())-1, mcurrentTest)];

        // texturas
        if (test->textureFilename0.size()) mtextureWidget->uploadTextureImage(0, test->textureFilename0.c_str());
        if (test->textureFilename1.size()) mtextureWidget->uploadTextureImage(1, test->textureFilename1.c_str());
        if (test->textureFilename2.size()) mtextureWidget->uploadTextureImage(2, test->textureFilename2.c_str());
        if (test->textureFilename3.size()) mtextureWidget->uploadTextureImage(3, test->textureFilename3.c_str());

	// mesh
	
	if (test->meshFilename.size() && msceneWidget->m_meshModel) 
	  msceneWidget->m_meshModel->loadObjModel(test->meshFilename.data());

        // escena
	msceneWidget->m_scene->setCurrentModel( msceneWidget->m_models[ test->sceneIndex ] );
        msceneWidget->checkWireframe( test->wireframe? 1: 0);

        // color de fondo
        msceneWidget->m_scene->setClearColor(vec4_t(test->backgroundRed, test->backgroundGreen, test->backgroundBlue, 0.9f));

	//vertex density (subdivision level for plane and cube)
	msceneWidget->setVertexDensity(test->vertexDensity);
#if DEBUG
	std::cerr << "processed preFrame() for test " << mcurrentTest << std::endl;
	std::cerr << "   used model " << test->sceneIndex << endl;
	if (test->textureFilename0.size()) std::cerr << "   loaded texture '" << 
					     test->textureFilename0 << "' in unit 0" << std::endl;
	if (test->textureFilename1.size()) std::cerr << "   loaded texture '" << 
					     test->textureFilename1 << "' in unit 1" << std::endl;
	if (test->textureFilename2.size()) std::cerr << "   loaded texture '" << 
					     test->textureFilename2 << "' in unit 2" << std::endl;
	if (test->textureFilename3.size()) std::cerr << "   loaded texture '" << 
					     test->textureFilename3 << "' in unit 3" << std::endl;
#endif
    }
}


void Corrector::setTextureWidget(CTextureWidget* textureWidget)
{
    mtextureWidget = textureWidget;
}
