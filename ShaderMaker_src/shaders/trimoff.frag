// simple fragment shader
uniform vec2 sm_vpsize;

// 'time' contains seconds since the program was linked.
uniform float time;

void main()
{
	if (gl_FragCoord.x < sm_vpsize.x/2. &&
		gl_FragCoord.y > sm_vpsize.y/2.) discard;
	gl_FragColor = gl_Color;
}
