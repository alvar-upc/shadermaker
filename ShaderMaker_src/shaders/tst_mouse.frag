// simple fragment shader

// 'time' contains seconds since the program was linked.
uniform float time;
uniform ivec2 sm_mouse;
uniform vec2 sm_vpsize;

void main()
{
	gl_FragColor = vec4(vec2(sm_mouse)/sm_vpsize, 1.0, 1.0)*gl_Color;
}
