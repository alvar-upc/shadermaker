Warning!!!

qmake does not seem to add properly the CoreFoundation framework, and thus fails to link.

manually edit the Makefile and add -framework CoreFoundation to overcome, albeit this needs to be done each time we do qmake!

To make this permanent, edit /opt/local/share/qt4/mkspecs/common/mac.conf and add at the end of "QMAKE_LIBS_OPENGL	= -framework OpenGL -framework AGL", "-framework CoreFoundation", and so it will add it every time (but patching needs to be repeated when we update qmake).
