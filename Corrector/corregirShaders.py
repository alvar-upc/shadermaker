#! /usr/bin/python3

import os, subprocess, glob, optparse, re
#
#  Customization variables:     ----------------------------------------------------------
#
# Info pblm:
baseName      = 'spherize2'
FLAGS         = 'VGE'
PRACTICA      = "P29243_"
#
# Info Host config:
SHADERMAKER   = "/home/alvar/Code/shadermaker/ShaderMaker_src/ShaderMaker"
#SHADERMAKER   = "/Users/alvarv/Code/shadermaker/ShaderMaker_src/ShaderMaker.app/Contents/MacOS/ShaderMaker"
debug         = False
#ERROR_STRING  = 'ERROR: '     # Mac w. driver '2.1 ATI-1.22.25'
#WARNING_STRING= 'WARNING: '   # Mac w. driver '2.1 ATI-1.22.25'
ERROR_STRING  = ' error '     # linux w. nvidia driver
WARNING_STRING= ' warning '   # linux w. nvidia driver
latex_exe     = '/usr/bin/pdflatex'
#
# No changes should be needed beyond this point.  ----------------------------------------
#
# Let's compute where things are...
ARREL           = os.getcwd()
DIRBIN          = os.path.dirname(os.path.abspath(__file__)) # where to find other files of the package
DIRCorr         = ARREL+"/CORRECTES/"+baseName.title()
print(u"Agafant versio de referencia de %s\n"%(DIRCorr))
TESTFILE        = DIRCorr + '/' + baseName + '.test'
SHADER          = baseName
latex_template  = DIRBIN + '/report.tex'
report_title	= 'Control de laboratori - '+baseName.title()
tmp_latex_file  = baseName+'-report.tex'
tmp_log_file    = 'log.txt'
vertex_shader   = ''
fragment_shader = ''
geometry_shader = ''
this_vs         = ''
this_gs         = ''
this_fs         = ''

#######################
#
# fix paths in testfile so they point to correctsols dir:
#
def fixTestFile(maintst):
    StTest = maintst
    f = open(maintst, 'r')
    s = f.read()
    f.close()
    #print('==================== 1:\n', s)
    s,numreplu = re.subn(r'(unit[0-4])\s+(\'|\")', r'\1 \2'+DIRCorr+'/', s)
    #print('==================== 2:\n', numrepl, s)
    s,numreplm = re.subn(r'mesh\s+(\'|\")', r'mesh \1'+DIRCorr+'/', s)
    #print('==================== 2:\n', numreplu, numreplm, '\n', s)
    if numreplu + numreplm > 0:
        StTest = os.getcwd()+SHADER+'.test'
        f = open(StTest, 'w')
        f.write(s)
        f.close()
    return StTest
    
#######################
#
#  tryStudentsShaders: executes shader with tests specified in TESTFILE
#  and flags FLAGS, and returns compilation log as string.
#
def tryStudentsShaders(studentName):
    # fix testfile:
    MYTEST = fixTestFile(TESTFILE)
    #print("------------", MYTEST)
    cmd = [SHADERMAKER, MYTEST, SHADER, FLAGS]
    try:
        compileLog = subprocess.check_output(cmd, universal_newlines=True)
    except subprocess.CalledProcessError as err:
        print("ERROR: Executing "+err.cmd)
        print("       returned code %d\n" % (err.returncode))
    compilesOK = True
    if compileLog.count(ERROR_STRING):
        print(studentName+"'s shader does not compile")
        compileLog = compileLog+" SHADER COMPILATION ERROR"
        compilesOK = False
    elif compileLog.count(WARNING_STRING):
        print(studentName+"'s shader generates warnings")
        compileLog = compileLog+" COMPILES WITH WARNINGS"
    else:
        print(" Compilation OK")
    if debug: print(compileLog)
    return compileLog, compilesOK


#######################
#
#  Compute directories to process;
#  return as list
def buildTaskList():
    ENTREGADES = glob.glob(PRACTICA+'*')
    if debug:
        print("Anem a corrgir %d practiques:\n" % (len(ENTREGADES)))
        print(ENTREGADES)
    return ENTREGADES

#######################
#
#  del script de Carlos:
#
def checkSubmission(studentName):
    global vertex_shader
    global geometry_shader
    global fragment_shader
    global this_vs
    global this_gs
    global this_fs
    
    ok = True
    print(" Submitted files: ", glob.glob("*.vert"), glob.glob("*.geom"), glob.glob("*.frag"))
    
    if len(vertex_shader)>0 :
        files = glob.glob(baseName+"*.vert")
        if len(files)==0:
            print(studentName+" provided no vertex shader")
            ok = False
        elif len(files)==1:
            this_vs = files[0]
            if this_vs==vertex_shader:
                print(" Vertex shader exists")
            else:
                print(studentName+" provided no "+vertex_shader+' but '+ this_vs + ".")
                ok = False
        else:   
            print(studentName+" provided more than one vertex shader. Not evaluated!")
            ok=False
           
            
    if ok and len(geometry_shader)>0 :
        files = glob.glob(baseName+"*.geom")
        if len(files)==0:
            print(studentName+" provided no geometry shader.")
            ok = False
        elif len(files)==1:
            this_gs = files[0]
            if this_gs==geometry_shader:
                print(" Geometry shader exists")
            else:
                print(studentName+" provided no "+geometry_shader+' but '+ this_gs + ".")
                ok = False
        else:   
            print(studentName+" provided more than one geometry shader. Not evaluated!")
            ok=False

    if ok and len(fragment_shader)>0 :
        files = glob.glob(baseName+"*.frag")
        if len(files)==0:
            print(studentName+" provided no fragment shader.")
            ok = False
        elif len(files)==1:
            this_fs = files[0]
            if this_fs==fragment_shader:
                print(" Fragment shader exists")
            else:
                print(studentName+" provided no "+fragment_shader+' but '+ this_fs + ".")
                ok = False
        else:   
            print(studentName+" provided more than one fragment shader. Not evaluated!")
            ok=False
    return ok

def saveLogToFile(log, tmp_log_file):
    log=log.replace("\n\n","\n").replace("\n\n","\n")
    f = open(tmp_log_file,'w')
    f.write(log[2:])
    f.close()

#
#   Generar el LaTeX d'un estudiant:
#
def generateLatexReport(studentName, log, compilesOK):
    print(" Generating report...")
    # put log into a file
    saveLogToFile(log, tmp_log_file)
    # load latex template
    f = open(latex_template, 'r')
    s = f.read()
    f.close();
    # replace a few strings
    s=s.replace("TITLE", report_title)
    s=s.replace("AUTHOR", studentName)
    
    s=s.replace("SOLUTION1", DIRCorr+"/output1.png")
    s=s.replace("SOLUTION2", DIRCorr+"/output2.png")
    s=s.replace("SOLUTION3", DIRCorr+"/output3.png")
    s=s.replace("SOLUTION4", DIRCorr+"/output4.png")
    
    s=s.replace("XDIFFOUTPUT1", baseName+"diffs1.png")
    s=s.replace("XDIFFOUTPUT2", baseName+"diffs2.png")
    s=s.replace("XDIFFOUTPUT3", baseName+"diffs3.png")
    s=s.replace("XDIFFOUTPUT4", baseName+"diffs4.png")

    s=s.replace("DIFFOUTPUT1", baseName+"comp1.png")
    s=s.replace("DIFFOUTPUT2", baseName+"comp2.png")
    s=s.replace("DIFFOUTPUT3", baseName+"comp3.png")
    s=s.replace("DIFFOUTPUT4", baseName+"comp4.png")
   
    s=s.replace("OUTPUT1", baseName+"output1.png")
    s=s.replace("OUTPUT2", baseName+"output2.png")
    s=s.replace("OUTPUT3", baseName+"output3.png")
    s=s.replace("OUTPUT4", baseName+"output4.png")
    
    s=s.replace("LOGFILE", tmp_log_file)

    if len(vertex_shader)==0:
        s=s.replace("INCLUDE_VS_VALUE", "false")
    else:
        s=s.replace("INCLUDE_VS_VALUE", "true")
        s=s.replace("VERTEX_SHADER_FILE", vertex_shader)
        
    if len(geometry_shader)==0:
        s=s.replace("INCLUDE_GS_VALUE", "false")
    else:
        s=s.replace("INCLUDE_GS_VALUE", "true")
        s=s.replace("GEOMETRY_SHADER_FILE", geometry_shader)

    if len(fragment_shader)==0:
        s=s.replace("INCLUDE_FS_VALUE", "false")
    else:
        s=s.replace("INCLUDE_FS_VALUE", "true")
        s=s.replace("FRAGMENT_SHADER_FILE", fragment_shader)

    if compilesOK:
        s=s.replace("INCLUDE_OUTPUT_VALUE", "true")
    else:
        s=s.replace("INCLUDE_OUTPUT_VALUE", "false")
    
    # save latex file
    f = open(tmp_latex_file, 'w')
    f.write(s)
    f.close()

#######################
#
#  and compile the report:
def compileLatexReport(studentName):
    # compile latex file
    print(" Compiling latex file...")
    cad = tmp_latex_file[0:len(tmp_latex_file)-4]+".pdf"
    if os.path.exists(cad):
        os.remove(cad)
    cmd = [latex_exe, tmp_latex_file]
    if debug: print(cmd)
    out = ""
    try:
        out = subprocess.check_output(cmd,shell=False, universal_newlines=True)
    except:
        #out = str(out).replace("\\n",'\n').replace("\\r",'\r')
        print(out)
        print(" Latex compilation FAILED!")
        print("Latex file for "+studentName+" didn't compile:"+out)
        pass
    #print(cad)
    if os.path.exists(cad):
        print(" Latex compilation OK")    


#######################
# MAIN ENTRY POINT:
#
def main():
    global vertex_shader
    global geometry_shader
    global fragment_shader
    if FLAGS.count('V'): 
        vertex_shader = baseName + '.vert'
    if FLAGS.count('G'): 
        geometry_shader = baseName + '.geom'
    if FLAGS.count('F'): 
        fragment_shader = baseName + '.frag'

    parser = optparse.OptionParser( version = "%prog 1.0", usage = "%prog [option]", description = "", )

    #parser.add_option("", "--checkfiles", help="check existence of shader files", action="store_true")
    parser.add_option("", "--executefiles", help="run shaders with shadermaker", action="store_true")
    # Parse options 
    (options, args) = parser.parse_args()
    execute = options.executefiles
        
    #
    # let's do real work:
    ENTREGADES = buildTaskList()
    ENTREGADES.sort()
    # Loop over the different student's work, computing the images to compare with
    # the reference images; then generate a report for the student.
    for Practica in ENTREGADES: # de moment faig sols el primer fins que funcioni...
        ALUMNE = Practica[7:].replace('.', ' ').replace('-', ' ').title()
        print("Processant entrega de ",ALUMNE)
        os.chdir(Practica+"/Entregats")
        ok = checkSubmission(ALUMNE)
        if ok and execute:
            log, compilesOK = tryStudentsShaders(ALUMNE)
            if compilesOK:
                for i in range(1,5):
                    file = "output"+str(i)+".png"
                    os.rename(file, SHADER+file)
                    #
                    # The following works only with a recent version of netpbm. 
                    # other image toolsets should be tested; perhaps one that is
                    # integrated into Python...?
                    #
                    # Compute image differences:
                    os.system("pngtopnm " + SHADER+file+ " | pnmarith -xor - " +
                        DIRCorr+"/output"+str(i)+".pnm | pnminvert | pnmtopng > " + SHADER+"comp"+str(i)+".png")
                    # Compute BW mask showing where there are differences:
                    os.system("pngtopnm " + SHADER+file+ " | pnmarith -xor - " +
                        DIRCorr+"/output"+str(i)+".pnm | ppmtopgm | pamthreshold -simple -threshold 0.003 " +
                           "| pnminvert | pnmtopng > " + SHADER+"diffs"+str(i)+".png")
            # Ready to generate this student's report:
            generateLatexReport(ALUMNE, log, compilesOK)
            compileLatexReport(ALUMNE)
 
        # ...
        os.chdir(ARREL)

def something():        
        for i in range(1,5):
            file = "output"+str(i)+".png"
            os.rename(file, SHADER+file)
            # generate comparisson images
            os.system("pngtopnm " + SHADER+file+ " | pnmarith -xor - " +
                DIRCorr+"/output"+str(i)+".pnm | pnminvert | pnmtopng > " + SHADER+"comp"+str(i)+".png")
            os.system("pngtopnm " + SHADER+file+ " | pnmarith -xor - " +
                DIRCorr+"/output"+str(i)+".pnm | ppmtopgm | pamthreshold -simple -threshold 0.003 " +
                   "| pnminvert | pnmtopng > " + SHADER+"diffs"+str(i)+".png")
            



if __name__ == "__main__":
    main()
