Requisits:
    Cal tenir instal·lats el ShaderMaker en versió corrector (aquest
    repo), les eines del netpbm, en una versió prou moderna (que
    inclogui pamthreshold), LaTeX, Python3, pdftk. 

Hypothesis: 

  * s'executa aquest script des d'un directori que conté totes les
    entregues dels estudiants. Haurien d'estar en directoris de nom
    'contingut de PRACTICA'_nom.del.estudiant/Entregats
  * El mateix directori tindrà un subdirectori CORRECTES amb un
    subdirectori amb el nom del shader (baseName.title()) amb la
    solució correcta, i l'arxiu de configuració baseName.test (veure
    més avall).
  * En el directori de la solució s'haurà executat ShaderMaker en mode
    correcció per a generar les imatges de referència (que es gravaran
    automàticament als arxius 'output[1-4].png'.
  * cal pre-calcular aquestes imatges en format .pnm amb
    	for f in 1 2 3 4; do
           pngtopnm output$f.png > output$f.pnm
        done
  * Al mateix directori poden deixar-se arxius de textura que el .test
    carregui, o models .obj que hi referenciï. El script de correcció
    tansformarà automàticament aquests noms en paths absoluts, així
    que han de ser relatius al directori de correcció. 
  * Cal assignar les variables de configuració, al principi del
    script: baseName, FLAGS i PRACTICA, a més del path a ShaderMaker i
    al pdflatex, i mirar que ERROR_STRING i WARNING_STRING siguin
    correctes per a la plataforma en què corregim.
  * Aleshores ja podem llençar el script de correcció, que entrarà a
    cada subdirectori d'alumne, i si ha entregat el problema que estem
    corregint, el provarà segons la configuració al .test, i generarà
    un informe baseName+'-report.pdf'.
  * Un cop acabats tots, podem generar un informe monolític amb 
        pdftk P*/Entregats/baseName-report.pdf cat output baseName-detailed.pdf
  * PROBLEMS:
    En molt comptades ocasions, ShaderMaker salva una finestra negra
    en comptes del que cal (és un event rar, i sembla un problema de
    sincronització, ne que la "foto" s'ha fet abans que OpenGL hagués
    fet el bufferSwitch...)

Sintaxi fitxer .test

  Els arxius .test consisteixen en un seguit de testos descrits amb la
  paraula test, seguida d'un nom, seguit d'un gafet que obre, una
  sèrie de paràmetres del test, i un gafet que tanca. Ara mateix cal
  fer exactament quatre tests de cada shader (perquè el script python
  ho suposa).

  Un exemple d'un test relativament complet és el següent (un arxiu
  estaria fet de quatre casos semblants:

  test dos
  {
    wire
    background 0.9 0.9 0.9
    scene 5		
    theta 60
    psi -120
    mesh "boid.obj"
    translate 0 0 -2.7
    unit0 'sphere1.png'
    int s 0 0 0 0
    float side 0.10 0 0 0   
  }

  Tingui's en compta que aquestes instruccions modifiquen l'estat del
  corrector. Si no es tornen a assignar, es propaguen a tests futurs
  del mateix arxiu. Per exemple, posar al primer test la paraula clau
  "wire" fa que tots es vegin en filferros, llevat que algun altre
  invoqui "solid"...

  Les paraules claus al començament de línia són:
    
    * background: fixa el color de background. Convé fer servir un de
      		  clar, com aquí, per a que es vegi millor en pdf o
      		  paper...
    * scene: 	  El tipus d'escena que es vol. Correspon al selector
      		  "Test Model" de la pesatanya Scene del
      		  ShaderMaker. S'ha de donar un index enter, que
      		  correspon a la posició en aquell desplegable:
		    0: Single point 
		    1: Plane
		    2: Cube
		    3: Sphere
		    4: Torus
		    5: Mesh
    * wire	  display in wireframe
    * solid	  display in solid (normal) mode (i.e. not wireframe)  

    * theta: 	  Camera rotation around y axis
    * psi:	  Camera rotation around x axis
    * phi: 	  Camera rotation around z axis  
    * translate x y z 
      		  Camera translation.
		  Per a fixar els paràmetres de la càmera, es pot
		  executar ShaderMaker en mode interactiu normal,
		  girar la càmera i fer zoom/pan ad libitum, i quan es
		  té una vista que es considera útil per a la
		  correcció, prèmer (amb el focus a la part gràfica de
		  la interfície) la tecla F1. Pel terminal,
		  ShaderMaker escriurà els valors de theta, psi, phi i
		  translate que reprodueixen exactament la vista
		  actual. Per a que tota la vista s'assembli al màxim,
		  convé reescalar a un viewport aproximadament quadrat
		  (en mode correcció farà servir un de 512x512).

 
    * mesh: 	  Ha d'anar seguit d'un string entre cometes o tildes,
      		  amb el nom d'un arxiu obj, relatiu al directori on
      		  viu el .test. És indispensable per als tests amb
      		  scene=5, almenys pel primer d'ells.
    * uniti:	  on i és 0, 1, 2, o 3, i indica a quina unitat de
      		  textura es vol carregar la textura continguda en el
      		  següent argument (que també és un nom d'arxiu entre
      		  cometes o tildes, i relatiu al directori de l'arxiu
      		  .test).
    * int nom a b c d:
                  dóna valor a un uniform de nom 'nom'. Es fan servir
      		  el nombre de components que corresponguin al uniform
      		  en el programa. Aquesta forma es fa servir també per
      		  a uniforms booleans i per a assignar una unitat de
      		  textura a un sampler2D (no es contemplen ara per ara
      		  samplers d'altres tipus).
    * float nom a b c d:
      	    	  Com l'anterior, però per a uniforms de coma flotant.
    * vertexDensity:
		  enter que indica el nivell de subdivisió de Plane i
      		  Cube. Pot valdre entre 1 i 9. Si val i, l'aresta
      		  d'aquestes primitives es subdividirà en 2^(i-1)
      		  parts iguals. 


En comptes de 'test', una línia pot contenir una comanda vs, fs o gs,
seguides d'un vertex, fragment o geometry shaders escrits en una sola
línia... (Untested)
